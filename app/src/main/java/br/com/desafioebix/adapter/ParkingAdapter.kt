
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.desafioebix.R
import br.com.desafioebix.model.ParkingResponse
import br.com.desafioebix.ui.ParkingDetailsActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_cardview_parking.view.*
import kotlinx.android.synthetic.main.activity_cardview_parking_detail_item.view.*
import kotlinx.android.synthetic.main.activity_cardview_parking_detail_item.view.textView_name
import kotlinx.android.synthetic.main.activity_cardview_parking_detail_item.view.textView_vicinity


class ParkingAdapter(var items : ArrayList<ParkingResponse>, var context : Context): RecyclerView.Adapter<ParkingAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_cardview_parking, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items.get(position))
        holder.itemView.setOnClickListener{
            try {

                var parkingResponse = items.get(position)

                context.startActivity(Intent(context, ParkingDetailsActivity::class.java).putExtra("myKey", parkingResponse))

            }catch (e : Exception){

            }
        }

    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {


        val txtVNome = itemView.textView_name
        val txtVVEndereco = itemView.textView_vicinity
        val txtVNota = itemView.textView_rating


        fun bind (parkingResponse: ParkingResponse){
            if(parkingResponse != null){
                txtVNome?.text = parkingResponse.name
                txtVVEndereco?.text = parkingResponse.vicinity
                txtVNota?.text = parkingResponse.rating.toString()

            }


        }
    }
}