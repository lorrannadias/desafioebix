package br.com.desafioebix.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Viewport : Serializable {

    @SerializedName("northeast")

    private val northeast: Northeast? = null
    @SerializedName("southwest")

    private val southwest: Southwest? = null


}