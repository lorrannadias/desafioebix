package br.com.desafioebix.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Photo : Serializable {

    @SerializedName("height")
    var height: Int? = null

    @SerializedName("html_attributions")
    var htmlAttributions: List<String>? = null

    @SerializedName("photo_reference")
    var photoReference: String? = null

    @SerializedName("width")
    var width: Int? = null

}