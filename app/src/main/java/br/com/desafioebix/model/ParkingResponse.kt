package br.com.desafioebix.model

import com.google.gson.annotations.SerializedName;
import java.io.Serializable

class ParkingResponse : Serializable {

    @SerializedName("geometry")
     val geometry: Geometry? = null

    @SerializedName("icon")
     val icon: String? = null

    @SerializedName("id")
     val id: String? = null

    @SerializedName("name")
     val name: String? = null

    @SerializedName("photos")
    var photos : ArrayList<Photo>? = null

    @SerializedName("opening_hours")
     val openingHours: OpeningHours? = null

    @SerializedName("place_id")
     val placeId: String? = null

    @SerializedName("plus_code")
     val plusCode: PlusCode? = null

    @SerializedName("rating")
     val rating: Double? = null

    @SerializedName("reference")
     val reference: String? = null

    @SerializedName("scope")
     val scope: String? = null

    @SerializedName("types")
     val types: ArrayList<String>? = null

    @SerializedName("user_ratings_total")
     val userRatingsTotal: Int? = null

    @SerializedName("vicinity")
     val vicinity: String? = null


}