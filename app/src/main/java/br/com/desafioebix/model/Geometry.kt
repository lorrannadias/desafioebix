package br.com.desafioebix.model


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable

class Geometry : Serializable{

    @SerializedName("location")

    private val location: Location? = null


    @SerializedName("viewport")

    private val viewport: Viewport? = null


}