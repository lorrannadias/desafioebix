package br.com.desafioebix.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class PlusCode : Serializable {

    @SerializedName("compound_code")

    private val compoundCode: String? = null
    @SerializedName("global_code")

    private val globalCode: String? = null

}