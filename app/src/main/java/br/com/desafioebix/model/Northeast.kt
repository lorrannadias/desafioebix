package br.com.desafioebix.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Northeast : Serializable {

    @SerializedName("lat")

    private val lat: Double? = null
    @SerializedName("lng")

    private val lng: Double? = null

}