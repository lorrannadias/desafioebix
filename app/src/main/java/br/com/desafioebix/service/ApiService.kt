package br.com.desafioebix.service


import android.text.Layout
import br.com.desafioebix.model.ParkingResponse
import com.google.gson.JsonObject
import retrofit2.Call

import retrofit2.http.GET
import retrofit2.http.Query

interface  ApiService {
    @GET("api/place/nearbysearch/json?")
    fun loadResults(@Query("location") location: String, @Query("radius") radius: String, @Query("type") type: String, @Query("key") key: String) :
            Call<JsonObject>

    @GET("directions/json")
    fun getDirections(@Query("origin") origin: String, @Query("destination") destination: String, @Query("key") key: String): Call<Layout.Directions>

}
