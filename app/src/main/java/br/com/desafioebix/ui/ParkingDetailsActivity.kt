package br.com.desafioebix.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import br.com.desafioebix.R
import br.com.desafioebix.model.ParkingResponse
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_cardview_parking_detail_item.*


class ParkingDetailsActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cardview_parking_detail_item)


        var parkingResponse : ParkingResponse
        parkingResponse = intent.getSerializableExtra("myKey") as ParkingResponse
        if (parkingResponse != null) {
            textView_name.text = parkingResponse.name
            textView_vicinity.text = parkingResponse.vicinity
            ratingBar.rating = parkingResponse.rating!!.toFloat()


            var photoreference = parkingResponse.photos?.get(0)?.photoReference

            var url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + photoreference + "&key=" + "AIzaSyAgwmYLwzI1fuDOizaok5QdeIqz2QeAxLQ"

            Picasso.get().load(url)
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.ic_noimage)
                .into(imageView_photo)

        }

        textView_vicinity.setOnClickListener{
            val intent = Intent(this@ParkingDetailsActivity, MapsActivity::class.java)
            intent.putExtra("key", "Kotlin")
            startActivity(intent)
        }



    }

}







