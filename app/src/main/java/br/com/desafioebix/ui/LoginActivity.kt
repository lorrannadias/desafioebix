package br.com.desafioebix.ui

import android.content.Intent
import android.os.Bundle
import android.view.View

import androidx.appcompat.app.AppCompatActivity

import android.widget.Button
import android.widget.EditText

import br.com.desafioebix.R
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity() : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        button_login.setOnClickListener {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.putExtra("key", "Kotlin")
            startActivity(intent)
        }


    }

}






