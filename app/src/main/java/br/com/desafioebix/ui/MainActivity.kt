package br.com.desafioebix.ui

import ParkingAdapter
import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.desafioebix.R
import br.com.desafioebix.model.ParkingResponse
import br.com.desafioebix.service.ApiService
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_cardview_parking.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jetbrains.anko.yesButton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity() : AppCompatActivity() {

    val PERMISSION_ID = 42

    var  parkingAdapter = ParkingAdapter(ArrayList(),this)

    companion object {
      lateinit var mLastLocation : Location
    }


    lateinit var mFusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        //   API do provedor de localização fundida real para obter a posição atual dos usuários
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLastLocation()


        recycleView_list.layoutManager = LinearLayoutManager(this)
        recycleView_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = parkingAdapter
        }

    }
     fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/maps/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


     fun Buscar(latitude: String, longitude: String) {
        doAsync {
            val call = getRetrofit().create(ApiService::class.java).loadResults("${latitude},${longitude}", "5000", "parking", "AIzaSyAgwmYLwzI1fuDOizaok5QdeIqz2QeAxLQ").execute()
            uiThread {
                if (call.errorBody() == null) {
                    val arrayList: ArrayList<ParkingResponse> = ArrayList<ParkingResponse>()
                    var myArray =  call.body().getAsJsonArray("results")
                    var parkingResponse = ParkingResponse()

                     myArray.forEach {
                         parkingResponse =  Gson().fromJson(it, ParkingResponse::class.java)
                         arrayList.add(parkingResponse)
                    }
                    parkingAdapter.items = arrayList
                    parkingAdapter.notifyDataSetChanged()
                } else {
                    showErrorDialog()
                }
            }

        }

    }

    private fun showErrorDialog() {
        alert("ocorreu um erro, tente de novo.") {
            yesButton { }
        }.show()
    }




    ////// Pegando a Logitude e Latitude

    fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }
    //Este método nos dirá se o usuário nos concede ou não acesso ACCESS_COARSE_LOCATIONe ACCESS_FINE_LOCATION.
    fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }
    //Este método solicitará as permissões necessárias ao usuário se elas ainda não tiverem sido concedidas.
    fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }
    //Esse método é chamado quando um usuário permite ou nega nossas permissões solicitadas. Portanto, isso nos ajudará a avançar se as permissões forem concedidas.
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // Granted. Start getting the location information
            }
        }
    }
    @SuppressLint("MissingPermission")
    fun requestNewLocationData() {

        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000

    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        findViewById<TextView>(R.id.latTextView).text = location.latitude.toString()
                        findViewById<TextView>(R.id.lonTextView).text = location.longitude.toString()

                        Buscar(location.latitude.toString(), location.longitude.toString())

                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)


            }
        } else {
            requestPermissions()
        }
    }

        private val mLocationCallback = object : LocationCallback() {
       override fun onLocationResult(locationResult: LocationResult) {
            mLastLocation = locationResult.lastLocation
           Buscar(mLastLocation.latitude.toString(), mLastLocation.longitude.toString())
           findViewById<TextView>(R.id.latTextView).text = mLastLocation.latitude.toString()
            findViewById<TextView>(R.id.lonTextView).text = mLastLocation.longitude.toString()
        }


    }

}

///aqui